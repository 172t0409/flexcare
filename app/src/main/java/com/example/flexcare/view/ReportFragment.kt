package com.example.flexcare.view

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.flexcare.databinding.FragmentReportBinding
import com.example.flexcare.viewmodel.ReportViewModel

class ReportFragment : Fragment() {

    private lateinit var reportViewModel: ReportViewModel
    private var _binding: FragmentReportBinding? = null

    private val binding get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        reportViewModel =
            ViewModelProvider(this).get(ReportViewModel::class.java)

        _binding = FragmentReportBinding.inflate(inflater, container, false)
        val root: View = binding.root


        return root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        reportViewModel = ViewModelProvider(this).get(ReportViewModel::class.java)
        // TODO: Use the ViewModel
    }

}