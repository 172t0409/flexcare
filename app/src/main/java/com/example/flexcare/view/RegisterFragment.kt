package com.example.flexcare.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.flexcare.databinding.FragmentRegisterBinding
import com.example.flexcare.model.FoodAdapter
import com.example.flexcare.viewmodel.RegisterViewModel

class RegisterFragment : Fragment() {

    private lateinit var registerViewModel: RegisterViewModel
    private var _binding: FragmentRegisterBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    private lateinit var adapter : FoodAdapter


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        registerViewModel =
            ViewModelProvider(this).get(RegisterViewModel::class.java)

        _binding = FragmentRegisterBinding.inflate(inflater, container, false)
        val root: View = binding.root

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init(view)
    }


    private fun init(view: View){
        val name: EditText = binding.txtName
        val protein: EditText = binding.txtProtein
        val carbs: EditText = binding.txtCarbs
        val fat : EditText = binding.txtFat

        binding.btnRegistrar.setOnClickListener { registerViewModel.addFood(name.text.toString(), protein.text.toString(), carbs.text.toString(), fat.text.toString()) }
        initRecyclerView(view)
    }

    private fun initRecyclerView(view: View) {
        adapter = FoodAdapter(view)
        binding.rvFoods.layoutManager = LinearLayoutManager(view.context)
        binding.rvFoods.adapter = adapter
        registerViewModel.listaUsuarios.observe(viewLifecycleOwner, Observer {
            adapter.setFoodList(it)
            adapter.notifyDataSetChanged()
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}