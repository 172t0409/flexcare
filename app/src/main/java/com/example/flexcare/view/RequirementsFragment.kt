package com.example.flexcare.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.SeekBar
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.flexcare.databinding.FragmentRequirementsBinding
import com.example.flexcare.viewmodel.RequirementsViewModel

class RequirementsFragment : Fragment() {

    private lateinit var requirementsViewModel: RequirementsViewModel
    private var _binding: FragmentRequirementsBinding? = null

    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater,container: ViewGroup?, savedInstanceState: Bundle?): View? {
        requirementsViewModel = ViewModelProvider(this).get(RequirementsViewModel::class.java)

        _binding = FragmentRequirementsBinding.inflate(inflater, container, false)
        val root: View = binding.root
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init(view)
    }


    private fun init(view: View){
        requirementsViewModel.requirementsModel.observe(viewLifecycleOwner, Observer {
            binding.txtlvl.text = it.lvl
        })
        val weight: EditText = binding.txtPeso
        val length: EditText = binding.txtEstatura
        val age: EditText = binding.txtEdad
        var activityLvl = 0
        binding.barraActividad.max = 4
        binding.barraActividad.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {

            override fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean) {
                activityLvl = i
                requirementsViewModel.onProgressChanged(seekBar,i,b)
            }
            override fun onStartTrackingTouch(seekBar: SeekBar) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {
            }
        })
        binding.btnCalcular.setOnClickListener {
            requirementsViewModel.calculatingCalories(weight.text.toString(), length.text.toString(), age.text.toString(), checkGender(), activityLvl)
            binding.txtPeso.setText("")
            binding.txtEstatura.setText("")
            binding.txtEdad.setText("")
        }
    }

    fun checkGender(): Int{
        if (binding.rbHombre.isChecked)
            return 0
        else
            return 1
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


}