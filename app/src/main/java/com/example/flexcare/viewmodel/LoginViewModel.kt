package com.example.flexcare.viewmodel

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.flexcare.model.LoginModel
import com.example.flexcare.model.UserPrefs
import com.example.flexcare.view.MenuView
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthCredential
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase

class LoginViewModel : ViewModel() {
    private val GOOGLE_SIGN_IN = 100
    private lateinit var googleSingInClient: GoogleSignInClient
    private val db = FirebaseFirestore.getInstance()

    val loginModel = MutableLiveData<LoginModel>()

    companion object{
        lateinit var prefs: UserPrefs
    }

    fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?, context: Context) {
        prefs = UserPrefs(context)
    }

    fun signIn(activity: Activity) {
        val googleSignInOptions: GoogleSignInOptions =
            GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken("961034533886-9lkbsm6m9qhfgr9ecda5cnt3n984te2o.apps.googleusercontent.com")
                .requestEmail()
                .build()
        googleSingInClient = GoogleSignIn.getClient(activity, googleSignInOptions)
        googleSingInClient.signOut()

        val intent = googleSingInClient.signInIntent
        activity.startActivityForResult(intent, GOOGLE_SIGN_IN)
    }
    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?, activity: Activity, context: Context) {
        var uid: String = ""
        var name: String = ""
        var email: String = ""
        if (requestCode == GOOGLE_SIGN_IN) {
            val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account: GoogleSignInAccount? = task.getResult(ApiException::class.java)

                if (account != null) {
                    val credential: AuthCredential =
                        GoogleAuthProvider.getCredential(account.idToken, null)
                        FirebaseAuth.getInstance().signInWithCredential(credential)
                        .addOnCompleteListener {
                            if (it.isSuccessful) {
                                val user = Firebase.auth.currentUser
                                user?.let {
                                    uid = user.uid
                                    name = user.displayName!!
                                    email = user.email!!
                                    db.collection(email).document("requerimientos").get()
                                        .addOnSuccessListener {
                                            if (it.exists()) {
                                                println("Existe")
                                            } else {
                                                db.collection(email).document("requerimientos").set(
                                                    hashMapOf(
                                                        "calorias" to 0,
                                                        "proteina" to 0,
                                                        "carbohidratos" to 0,
                                                        "grasa" to 0
                                                    )
                                                )
                                                db.collection(email).document("consumidos").set(
                                                    hashMapOf(
                                                        "calorias" to 0,
                                                        "proteina" to 0,
                                                        "carbohidratos" to 0,
                                                        "grasa" to 0
                                                    )
                                                )
                                                db.collection(email).document("alimentos consumidos").set(
                                                    hashMapOf(
                                                        "nombre" to null
                                                    )
                                                )
                                            }

                                        }

                                }


                                val intent = Intent(context, MenuView::class.java)
                                activity.startActivity(intent)

                            } else {
                                println("NOOOOOOO")
                            }
                        }
                }

            } catch (e: ApiException) {

            }

        }
    }

}