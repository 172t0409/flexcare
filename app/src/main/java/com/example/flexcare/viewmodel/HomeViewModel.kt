package com.example.flexcare.viewmodel

import androidx.lifecycle.*
import com.example.flexcare.model.HomeModel
import com.example.flexcare.model.HomeProvider


class HomeViewModel : ViewModel() {

    private val homeProvider = HomeProvider()

    val homeModel = MutableLiveData<HomeModel>()
    val isLoading = MutableLiveData<Boolean>()


    private fun setData() : LiveData<MutableList<HomeModel>> {
        val datos = MutableLiveData<MutableList<HomeModel>>()
            homeProvider.getData().observeForever {
                datos.value = it
            }
            println(datos.value?.get(0))



        return datos
    }

    private val _text = MutableLiveData<String>().apply {

    }

    val text: LiveData<String> = _text
    val listaUsuarios: LiveData<MutableList<HomeModel>> =setData()


}

