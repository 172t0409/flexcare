package com.example.flexcare.viewmodel


import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.flexcare.model.FoodModel
import com.example.flexcare.model.RegisterProvider

class RegisterViewModel : ViewModel() {

    private val registerProvider = RegisterProvider()

    private fun _getConsumedFoods() : LiveData<MutableList<FoodModel>>{
        val datos = MutableLiveData<MutableList<FoodModel>>()
        registerProvider.showList().observeForever{
            datos.value = it
        }
        return datos
    }

    fun addFood(name: String, protein: String, carbs: String, fat: String){
        RegisterProvider.addFood(name, protein.toInt(), carbs.toInt(), fat.toInt())
    }

    private val _text = MutableLiveData<String>().apply {
        value = "USUARIO FRAGMENT"
    }

    val text: LiveData<String> = _text
    val listaUsuarios: LiveData<MutableList<FoodModel>> = _getConsumedFoods()


}