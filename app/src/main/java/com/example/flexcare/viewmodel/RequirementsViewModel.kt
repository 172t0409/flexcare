package com.example.flexcare.viewmodel

import android.widget.SeekBar
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.flexcare.model.RequirementsModel
import com.example.flexcare.model.RequirementsProvider
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase

class RequirementsViewModel : ViewModel() {

    val requirementsModel = MutableLiveData<RequirementsModel>()
    private val db = FirebaseFirestore.getInstance()

    fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean){
        val currentRequirements : RequirementsModel = RequirementsProvider.activityLevel(i)
        requirementsModel.postValue(currentRequirements)
    }

    fun calculatingCalories(weight: String, length: String, age: String, gender: Int, actLvl: Int){
        var tmb: Double
        var calories: Int = 0
        var protein: Int
        var carbs: Int
        var fat: Int


        if (gender==0){
            tmb = 66 + (13.7 * weight.toDouble()) + (5 * length.toDouble()) - (6.75 * age.toDouble())
            if (actLvl==0){
                calories = Math.round(tmb*1.2).toInt()
            }else if (actLvl==1){
                calories = Math.round(tmb*1.375).toInt()
            }else if (actLvl==2){
                calories = Math.round(tmb*1.55).toInt()
            }else if (actLvl==3){
                calories = Math.round(tmb*1.72).toInt()
            }else if (actLvl==4){
                calories = Math.round(tmb*1.9).toInt()
            }

        }
        else{
            tmb = 665 + (9.6 * weight.toDouble()) + (1.8 * length.toDouble()) - (4.7 * age.toDouble())
            if (actLvl==0){
                calories = Math.round(tmb*1.2).toInt()
            }else if (actLvl==1){
                calories = Math.round(tmb*1.375).toInt()
            }else if (actLvl==2){
                calories = Math.round(tmb*1.55).toInt()
            }else if (actLvl==3){
                calories = Math.round(tmb*1.72).toInt()
            }else if (actLvl==4){
                calories = Math.round(tmb*1.9).toInt()
            }
        }

        protein = weight.toInt() * 2
        calories-= (protein*4)
        fat = Math.round(weight.toInt()*0.8).toInt()
        calories-= (fat*9)
        carbs = Math.round((calories/4).toDouble()).toInt()
        calories = (protein*4)+(carbs*4)+(fat*9)

        val user = Firebase.auth.currentUser
        user?.let {
            val email = user.email
            db.collection(email!!).document("requerimientos").set(
                hashMapOf(
                    "calorias" to calories,
                    "proteina" to protein,
                    "carbohidratos" to carbs,
                    "grasa" to fat
                )
            )

        }



    }

}