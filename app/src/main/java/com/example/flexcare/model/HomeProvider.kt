package com.example.flexcare.model

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.*
import kotlinx.coroutines.tasks.await

class HomeProvider {

        private val db = FirebaseFirestore.getInstance()
        private var listaUsuarios = mutableListOf<HomeModel>()
        private var objetivo: Int = 0
        private var consumidas: Int = 0
        private var restantes: Int = 0
        private var home = listOf<HomeModel>()





        suspend fun getRequirements(reference: String): Int {

            var a = db.collection(reference).document("requerimientos").get().await()
            return a.getLong("calorias")?.toInt()!!
        }

        suspend fun getConsumed(reference: String): Int {
            var a = db.collection(reference).document("consumidos").get().await()
            return a.getLong("calorias")?.toInt()!!
        }


        fun getData(): LiveData<MutableList<HomeModel>>{
            val datos = MutableLiveData<MutableList<HomeModel>>()
            val listaDatos = mutableListOf<HomeModel>()
                var rest =  getRequirements(getReference())-getConsumed(getReference())
                println("Objetivo: ${suspend{getRequirements(getReference())}} Consumidas: ${suspend { getConsumed(getReference()) }} Restantes: $rest")
            println("Objetivo: ${suspend{getRequirements(getReference())}} Consumidas: ${suspend { getConsumed(getReference()) }} Restantes: $rest")
            println("Objetivo: ${suspend{getRequirements(getReference())}} Consumidas: ${suspend { getConsumed(getReference()) }} Restantes: $rest")
            println("Objetivo: ${suspend{getRequirements(getReference())}} Consumidas: ${suspend { getConsumed(getReference()) }} Restantes: $rest")
                val usuario = HomeModel(objetivo, consumidas, restantes)
                listaDatos.add(usuario)

                datos.value = listaDatos
                listaUsuarios = listaDatos


            return datos

        }



        fun getReference(): String{
            val user = Firebase.auth.currentUser
            user?.let {
                return user.email!!
            }
            return ""
        }





}