package com.example.flexcare.model

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.view.menu.ActionMenuItemView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.flexcare.R
import com.example.flexcare.databinding.ItemBinding
import kotlinx.android.synthetic.main.item.view.*


class FoodAdapter(private val view: View) : RecyclerView.Adapter<FoodAdapter.FoodViewHolder>() {

    private var foodList = mutableListOf<FoodModel>()

    fun setFoodList(data: MutableList<FoodModel>){
        foodList = data
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FoodViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return FoodViewHolder(layoutInflater.inflate(R.layout.item, parent, false))
    }

    override fun onBindViewHolder(holder: FoodViewHolder, position: Int) {
        val food = foodList[position]
        holder.bindView(food)
    }

    override fun getItemCount(): Int {
        return if (foodList.size>0) foodList.size else 0
    }

    inner class FoodViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val binding = ItemBinding.bind(itemView)
        private lateinit var food: FoodModel

        fun bindView(food: FoodModel) {
            this.food = food
            binding.txtAlimento.text = "Alimento: ${food.name}"
            binding.txtCalorias.text = "Calorias: ${food.calories}"
        }
    }

    interface IURecycler {
        fun mostrarDialog(itemView: View, usuario:FoodModel)
        fun actualizarUsuario(itemView: View, usuario: FoodModel)

    }

}