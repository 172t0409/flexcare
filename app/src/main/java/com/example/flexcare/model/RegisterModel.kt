package com.example.flexcare.model

data class RegisterModel(var name: String, var calories: Int, var protein: Int, var carbs: Int, var fat: Int) {
}