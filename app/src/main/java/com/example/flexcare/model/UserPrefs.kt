package com.example.flexcare.model

import android.content.Context

class UserPrefs (val context: Context) {

    val SHARED_NAME = "User"
    val SHARED_USER_UID = "uid"
    val SHARED_USER_NAME = "name"
    val SHARED_USER_EMAIL = "email"

    val storage = context.getSharedPreferences(SHARED_NAME, 0)

    fun saveUid(name: String){
        storage.edit().putString(SHARED_USER_UID, name).apply()
    }

    fun saveName(name: String){
        storage.edit().putString(SHARED_USER_NAME, name).apply()
    }

    fun saveEmail(name: String){
        storage.edit().putString(SHARED_USER_EMAIL, name).apply()
    }

    fun getUid():String{
        return storage.getString(SHARED_USER_UID, "")!!
    }

    fun getName():String{
        return storage.getString(SHARED_USER_NAME, "")!!
    }

    fun getEmail():String{
        return storage.getString(SHARED_USER_EMAIL, "")!!
    }
}