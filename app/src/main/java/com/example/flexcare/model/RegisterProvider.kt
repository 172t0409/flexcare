package com.example.flexcare.model

import android.util.Log
import android.widget.ListView
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import com.google.firebase.ktx.Firebase

class RegisterProvider {
    companion object{
        lateinit var foodList: MutableList<FoodModel>

        private val db = FirebaseFirestore.getInstance()



        fun getReference(): String{
            val user = Firebase.auth.currentUser
            user?.let {
                return user.email!!
            }
            return ""
        }

        fun addFood(name: String, protein: Int, carbs: Int, fat: Int){
            db.collection(getReference()).document("consumidos").collection("alimentos")
                .document(name).set(
                hashMapOf(
                    "nombre" to name,
                    "calorias" to (protein*4)+(carbs*4)+(fat*9),
                    "proteina" to protein,
                    "carbohidratos" to carbs,
                    "grasa" to fat
                )
            )
            updateConsumed(protein,carbs,fat)
        }

        fun updateConsumed(protein: Int, carbs: Int, fat: Int){
            val consumedRef = db.collection(getReference()).document("consumidos")
            db.runTransaction {
                val snapshot = it.get(consumedRef)
                val newCalories = snapshot.getLong("calorias")!! + (protein*4)+(carbs*4)+(fat*9)
                val newProtein = snapshot.getLong("proteina")!! + protein
                val newCarbs = snapshot.getLong("carbohidratos")!! + carbs
                val newFat = snapshot.getLong("grasa")!! + fat
                it.update(consumedRef, "calorias", newCalories)
                it.update(consumedRef, "proteina", newProtein)
                it.update(consumedRef, "carbohidratos", newCarbs)
                it.update(consumedRef, "grasa", newFat)
            }
        }


    }

    fun showList() : LiveData<MutableList<FoodModel>>{
        var datos = MutableLiveData<MutableList<FoodModel>>()
        db.collection(getReference()).document("consumidos").collection("alimentos")
            .get()
            .addOnSuccessListener {
                val listData = mutableListOf<FoodModel>()
                for (document in it) {
                    var food = FoodModel(
                        document.getString("nombre")!!,
                        document.getLong("calorias")!!.toInt()
                    )
                    listData.add(food)
                }
                datos.value = listData
                foodList =listData
            }
            .addOnFailureListener{
                Log.e("firebase", "Error obteniendo los datos", it)
            }
        return datos
    }
}