package com.example.flexcare.vo

import java.lang.Exception

sealed class HomeResource <out T> {
    class Loading<out T> : HomeResource<T>()
    data class Success<out T>(val data: T) : HomeResource<T>()
    data class Failure<out T>(val exception: Exception): HomeResource<T>()
}